var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors =  require('cors')
const mongoose = require('mongoose')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const user = require("./routes/user");
const test = require("./routes/test");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.post('/user',user.register);
app.post('/user/login',user.login);
app.get('/user/:id', user.userInfo);
app.put('/user/:id', user.editInfo);
app.delete('/user/:id', user.deleteUser);

app.post('/test',test.addTest);
app.get('/test', test.findAll);
app.get('/test/:id', test.findOne);
app.delete('/test/:id', test.deleteTest);
app.put('/test/:id', test.giveGrade);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
const dotenv = require('dotenv')
dotenv.config()
const uri = `${process.env.MONGO_URI}${process.env.MONGO_DB}`
console.log(uri)
mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

var db = mongoose.connection

db.on('error', (err) => {
  console.log('connection error', err)
})
db.once('open', function () {
  console.log('connected to database')
})

module.exports = app;

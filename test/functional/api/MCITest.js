const chai = require("chai")
const expect = chai.expect
const request = require("supertest")
const MongoMemoryServer = require("mongodb-memory-server").MongoMemoryServer
const User = require("../../../models/user")
const Test = require("../../../models/test")
const mongoose = require("mongoose")
mongoose.set("useFindAndModify", false)
const {MongoClient} = require('mongodb')
const _ = require("lodash")

let server
let mongod
let db, validID,validIDT,url,connection

describe("MCI_TEST", () => {
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: "./test/database",
                    dbName: "MCItest" // by default generate random dbName
                }
            })
            // Async Trick - this ensures the database is created before
            // we try to connect to it or start the server
            url = await mongod.getConnectionString()

            connection = await MongoClient.connect(url, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            db = connection.db(await mongod.getDbName())
            server = require("../../../bin/www")
        } catch (error) {
            console.log(error)
        }
    })

    // after(async () => {
    //     try {
    //         await mongoose.connection.db.dropDatabase();
    //     } catch (error) {
    //         console.log(error);
    //     }
    // });

    beforeEach(async () => {
        try {
            await User.deleteMany({})
            let user = new User()
            user.username = "Jessie"
            user.password = "123456"
            user.email = "Jessie@gmail.com"
            user.gender = "female"
            user.age = 22
            user.medical_history = ""
            await user.save()
            user = new User()
            user.username = "Molly"
            user.password = "11111"
            user.email = "Molly@gmail.com"
            user.gender = "female"
            user.age = 50
            user.medical_history = "hypertension"
            await user.save()
            user = await User.findOne({ username: "Molly" })
            validID = user._id


            await Test.deleteMany({})
            let test = new Test()
            test.name = "test1"
            test.testername = "Jessie"
            test.testdate= "2019.11.01"
            test.grade = 72
            await test.save()
            test = new Test()
            test.name = "test2"
            test.testername = "Getty"
            test.testdate= "2019.10.22"
            test.grade = 66
            await test.save()
            test = await Test.findOne({ name: "test1" })
            validIDT = test._id


        } catch (error) {
            console.log(error)
        }
    })

    describe("User",()=>{
        describe("POST /user", () => {
            it("should return The username can not be empty", () => {
                const user = {
                    username: "",
                    password: "11111",
                    email: "zss@foxmail",
                    gender: "female",
                    age: 33,
                    medical_history: ""
                }
                return request(server)
                    .post("/user")
                    .send(user)
                    .then(res => {
                        expect(res.body.message).equals("The username can not be empty")
                    })
            })
            it("should return The password can not be empty", () => {
                const user = {
                    username: "Tony",
                    password: "",
                    email: "zss@foxmail",
                    gender: "male",
                    age: 43,
                    medical_history: "heart disease"
                }

                return request(server)
                    .post("/user")
                    .send(user)
                    .then(res => {
                        expect(res.body.message).equals("The password can not be empty")
                    })
            })
            it("should return The email can not be empty", () => {
                const user = {
                    username: "Tony",
                    password: "222",
                    email: "",
                    gender: "male",
                    age: 43,
                    medical_history: "heart disease"
                }

                return request(server)
                    .post("/user")
                    .send(user)
                    .then(res => {
                        expect(res.body.message).equals("The email can not be empty")
                    })
            })
            it("should return The gender can not be empty", () => {
                const user = {
                    username: "Tony",
                    password: "222",
                    email: "1@w",
                    gender: "",
                    age: 43,
                    medical_history: "heart disease"
                }

                return request(server)
                    .post("/user")
                    .send(user)
                    .then(res => {
                        expect(res.body.message).equals("The gender can not be empty")
                    })
            })
            it("should return The age can not be empty", () => {
                const user = {
                    username: "Tony",
                    password: "222",
                    email: "1@w",
                    gender: "male",
                    age:"",
                    medical_history: "heart disease"
                }

                return request(server)
                    .post("/user")
                    .send(user)
                    .then(res => {
                        expect(res.body.message).equals("The age can not be empty")
                    })
            })
            it("should return Username already exists", () => {
                const user = {
                    username: "Molly",
                    password: "11111",
                    email: "Molly@gmail.com",
                    gender: "female",
                    age: 50,
                    medical_history: "hypertension"
                }

                return request(server)
                    .post("/user")
                    .send(user)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Username already exists")
                    })
            })
            it("should return Successfully registered and update mongodb", () => {
                const user = {
                    username: "April",
                    password: "11111",
                    email: "344@foxmail",
                    gender: "female",
                    age: 44,
                    medical_history: "allergy"
                }

                return request(server)
                    .post("/user")
                    .send(user)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Successfully registered")
                        validID = res.body.data._id
                    })
            })
            after(() => {
                return request(server)
                    .get(`/user/${validID}`)
                    .expect(200)
                    .then(res => {
                        expect(res.body[0]).to.have.property("username", "April")
                        expect(res.body[0]).to.have.property("password", "11111")
                    })
            })

        })
        describe("POST /user/login", () => {
            it("should return wrong password message", () => {
                const user = {
                    username: "Molly",
                    password: "1100000"
                }

                return request(server)
                    .post("/user/login")
                    .send(user)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Wrong Password")
                    })
            })
            it("should return username is not exist message", () => {
                const user = {
                    username: "Kate",
                    password: "1100000"
                }

                return request(server)
                    .post("/user/login")
                    .send(user)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Username is not exist")
                    })
            })
            it("should return Successfully login and update mongodb", () => {
                const user = {
                    username: "Molly",
                    password: "11111",
                }

                return request(server)
                    .post("/user/login")
                    .send(user)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Successfully logged in")
                    })
            })
        })
        describe("GET /user/:id", () => {
            describe("when the id is valid", () => {
                it("should return the matching user information", done => {
                    request(server)
                        .get(`/user/${validID}`)
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body[0]).to.have.property("username", "Molly")
                            expect(res.body[0]).to.have.property("password", "11111")
                            expect(res.body[0]).to.have.property("email", "Molly@gmail.com")
                            expect(res.body[0]).to.have.property("gender", "female")
                            expect(res.body[0]).to.have.property("age", 50)
                            expect(res.body[0]).to.have.property("medical_history", "hypertension")
                            done(err)
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return User not found message", done => {
                    request(server)
                        .get("/user/9999")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body.message).equals("User NOT Found!")
                            done(err)
                        })
                })
            })

        })
        describe("PUT /user/:id", () => {
            describe("when the id is valid", () => {
                it("should return user updated information", () => {
                    request(server)
                        .put(`/user/${validID}`)
                        .send({"password": "asdfgh","age": 51,})
                        .expect(200)
                        .then(resp => {
                            // expect(resp.body).to.include({
                            //     message: "User information updated!"
                            // })
                            expect(resp.body.data).to.have.property("password", "asdfgh")
                            expect(resp.body.data).to.have.property("age", 51)

                        })
                })
                after(() => {
                    request(server)
                        .get(`/user/${validID}`)
                        .expect(200)
                        .then(resp => {
                            expect(resp.body.data).to.have.property("password", "asdfgh")
                            expect(resp.body[0]).to.have.property("age", 51)
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return the User NOT Found! message", done => {
                    request(server)
                        .get("/user/66666666666")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body.message).equals("User NOT Found!")
                            done(err)
                        })
                })
            })
        })
    })
    describe("Test",()=>{
        describe("POST /test", () => {
            it("should return The test name can not be empty", () => {
                const test = {
                    name: "",
                    testername: "Peter",
                    testdate: "2000.10.01",
                    grade: 66
                }

                return request(server)
                    .post("/test")
                    .send(test)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("The test name can not be empty")
                    })
            })
            it("should return The test is already exist", () => {
                const test = {
                    name: "test1",
                    testername: "Jessie",
                    testdate: "2019.11.01",
                    grade: 72
                }

                return request(server)
                    .post("/test")
                    .send(test)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("The test is already exist")
                    })
            })
            it("should return Test Successfully added and update mongodb", () => {
                const test = {
                    name: "test3",
                    testername: "Peter",
                    testdate: "2019.03.01",
                    grade: 70
                }

                return request(server)
                    .post("/test")
                    .send(test)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Test Successfully added")
                        validIDT = res.body.data._id
                    })
            })
            after(() => {
                return request(server)
                    .get(`/test/${validIDT}`)
                    .expect(200)
                    .then(res => {
                        expect(res.body[0]).to.have.property("name", "test3")
                        expect(res.body[0]).to.have.property("testername", "Peter")
                        expect(res.body[0]).to.have.property("testdate", "2019-03-01T00:00:00.000Z")
                        expect(res.body[0]).to.have.property("grade", 70)
                    })
            })
        })
        describe("GET /test", () => {
            it("should GET all the tests", done => {
                request(server)
                    .get("/test")
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        try {
                            expect(res.body).to.be.a("array")
                            expect(res.body.length).to.equal(2)
                            let result = _.map(res.body, test => {
                                return {
                                    name: test.name,
                                    testername: test.testername,
                                    testdate: test.testdate,
                                    grade: test.grade
                                }
                            })
                            expect(result[0].name).equals("test1")
                            expect(result[1].name).equals("test2")
                            done()
                        } catch (e) {
                            done(e)
                        }
                    })
            })
        })
        describe("GET /test/:id", () => {
            describe("when the id is valid", () => {
                it("should return the matching test", done => {
                    request(server)
                        .get(`/test/${validIDT}`)
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body[0]).to.have.property("name", "test1")
                            expect(res.body[0]).to.have.property("testername", "Jessie")
                            expect(res.body[0]).to.have.property("testdate", "2019-11-01T00:00:00.000Z")
                            expect(res.body[0]).to.have.property("grade", 72)
                            done(err)
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return Test not found message", done => {
                    request(server)
                        .get("/test/34565676772")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body.message).equals("Test NOT Found!")
                            done(err)
                        })
                })
            })
        })
        describe("PUT /test/:id", () => {
            describe("when the id is valid", () => {
                it("should return a message and update the grade", () => {
                    return request(server)
                        .put(`/test/${validIDT}`)
                        .send({"grade": 72})
                        .expect(200)
                        .then(resp => {
                            expect(resp.body).to.include({
                                message: "Test Successfully graded!"
                            })
                            expect(resp.body.data).to.have.property("grade", 72)
                        })
                })
                after(() => {
                    return request(server)
                        .get(`/test/${validIDT}`)
                        .expect(200)
                        .then(resp => {
                            expect(resp.body[0]).to.have.property("grade", 72)
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return the Test NOT Found! message", done => {
                    request(server)
                        .get("/test/1000000020202")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body.message).equals("Test NOT Found!")
                            done(err)
                        })
                })
            })
        })
        describe("DELETE /test/:id", () => {
            describe("when the id is valid", () => {
                it("should return confirmation message and update database", () => {
                    request(server)
                        .delete(`test/${validIDT}`)
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .then(res => {
                            expect(res.body.message).equals("Novels Successfully Deleted!")
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return the NOT found message", () => {
                    request(server)
                        .delete("/test/1000000020202")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .then(res => {
                            expect(res.body.message).equals("Test NOT Found!")
                        })
                })
            })
        })


    })


})